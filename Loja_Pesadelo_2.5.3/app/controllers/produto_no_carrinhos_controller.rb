class ProdutoNoCarrinhosController < ApplicationController
  before_action :set_produto_no_carrinho, only: [:show, :edit, :update, :destroy]

  # GET /produto_no_carrinhos
  # GET /produto_no_carrinhos.json
  def index
    @produto_no_carrinhos = ProdutoNoCarrinho.all
  end

  # GET /produto_no_carrinhos/1
  # GET /produto_no_carrinhos/1.json
  def show
  end

  # GET /produto_no_carrinhos/new
  def new
    @produto_no_carrinho = ProdutoNoCarrinho.new
  end

  # GET /produto_no_carrinhos/1/edit
  def edit
  end

  # POST /produto_no_carrinhos
  # POST /produto_no_carrinhos.json
  def create
    @produto_no_carrinho = ProdutoNoCarrinho.new(produto_no_carrinho_params)

    respond_to do |format|
      if @produto_no_carrinho.save
        format.html { redirect_to @produto_no_carrinho, notice: 'Produto no carrinho was successfully created.' }
        format.json { render :show, status: :created, location: @produto_no_carrinho }
      else
        format.html { render :new }
        format.json { render json: @produto_no_carrinho.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /produto_no_carrinhos/1
  # PATCH/PUT /produto_no_carrinhos/1.json
  def update
    respond_to do |format|
      if @produto_no_carrinho.update(produto_no_carrinho_params)
        format.html { redirect_to @produto_no_carrinho, notice: 'Produto no carrinho was successfully updated.' }
        format.json { render :show, status: :ok, location: @produto_no_carrinho }
      else
        format.html { render :edit }
        format.json { render json: @produto_no_carrinho.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /produto_no_carrinhos/1
  # DELETE /produto_no_carrinhos/1.json
  def destroy
    @produto_no_carrinho.destroy
    respond_to do |format|
      format.html { redirect_to produto_no_carrinhos_url, notice: 'Produto no carrinho was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def remove
    @produto_no_carrinho = ProdutoNoCarrinho.find(params[:id])
    array = []
    current_user.carrinho.produto_no_carrinhos.each do |prod|
      if prod.id != @produto_no_carrinho.id
        array << prod
      end
    end
    if current_user.atleta? || current_user.admin?
      current_user.carrinho.preco_final -= @produto_no_carrinho.preco_1
    elsif current_user.associado?
      current_user.carrinho.preco_final -= @produto_no_carrinho.preco_2
    else
      current_user.carrinho.preco_final -= @produto_no_carrinho.preco_3
    end
    current_user.carrinho.save

    @produtos = Produto.all
    @produtos.each do |produto|
      if produto.nome == @produto_no_carrinho.nome
        produto.quantidade += 1
        produto.save
        break
      end
    end
    @produto_no_carrinho.destroy
    respond_to do |format|
      format.html { redirect_to carrinho_path(current_user.carrinho) + '/show_prods'}
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_produto_no_carrinho
      @produto_no_carrinho = ProdutoNoCarrinho.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def produto_no_carrinho_params
      params.require(:produto_no_carrinho).permit(:nome, :quantidade, :preco_1, :preco_2, :preco_3)
    end
end
