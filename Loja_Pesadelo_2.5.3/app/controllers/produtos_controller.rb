class ProdutosController < ApplicationController
  load_and_authorize_resource 
  before_action :set_produto, only: [:show, :edit, :update, :destroy]

  # GET /produtos
  # GET /produtos.json
  def index
    @produtos = Produto.all
  end

  # GET /produtos/1
  # GET /produtos/1.json
  def show
  end

  # GET /produtos/new
  def new
    @produto = Produto.new
  end

  # GET /produtos/1/edit
  def edit
  end

  # POST /produtos
  # POST /produtos.json
  def create
    @produto = Produto.new(produto_params)

    respond_to do |format|
      if @produto.save
        format.html { redirect_to @produto, notice: 'Produto was successfully created.' }
        format.json { render :show, status: :created, location: @produto }
      else
        format.html { render :new }
        format.json { render json: @produto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /produtos/1
  # PATCH/PUT /produtos/1.json
  def update
    respond_to do |format|
      if @produto.update(produto_params)
        format.html { redirect_to @produto, notice: 'Produto was successfully updated.' }
        format.json { render :show, status: :ok, location: @produto }
      else
        format.html { render :edit }
        format.json { render json: @produto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /produtos/1
  # DELETE /produtos/1.json
  def destroy
    @produto.destroy
    respond_to do |format|
      format.html { redirect_to produtos_url, notice: 'Produto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add 
    @produto = Produto.find(params[:id])
    @produto.quantidade -= 1
    @produto.save

    prod_temp = ProdutoNoCarrinho.new
    prod_temp.quantidade = 1
    prod_temp.preco_1 = @produto.preco_1
    prod_temp.preco_2 = @produto.preco_2
    prod_temp.preco_3 = @produto.preco_3
    prod_temp.nome = @produto.nome
    array = []
    current_user.carrinho.produto_no_carrinhos.each do |prod|
      array << prod
    end

    if array.empty?
      prod_new = ProdutoNoCarrinho.new
      array << prod_new
    end
    array << prod_temp
    current_user.carrinho.produto_no_carrinhos = array
    prod_temp.save
    if current_user.atleta? || current_user.admin?
      current_user.carrinho.preco_final += @produto.preco_1
    elsif current_user.associado?
      current_user.carrinho.preco_final += @produto.preco_2
    else
      current_user.carrinho.preco_final += @produto.preco_3
    end
    current_user.carrinho.save
    respond_to do |format|
      format.html { redirect_to produtos_url, notice:  'Produto comprado' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_produto
      @produto = Produto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def produto_params
      params.require(:produto).permit(:preco_1, :preco_2, :preco_3, :nome, :quantidade, :imagem)
    end
end
