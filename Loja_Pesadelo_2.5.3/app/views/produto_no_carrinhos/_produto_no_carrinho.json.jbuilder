json.extract! produto_no_carrinho, :id, :nome, :quantidade, :preco_1, :preco_2, :preco_3, :created_at, :updated_at
json.url produto_no_carrinho_url(produto_no_carrinho, format: :json)
