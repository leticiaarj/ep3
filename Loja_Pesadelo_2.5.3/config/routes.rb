Rails.application.routes.draw do
  resources :produto_no_carrinhos
  resources :carrinhos
  resources :produtos
  devise_for :users
  resources :users

  get 'carrinhos/:id/show_prods' => 'carrinhos#show_prods'
  get 'produtos/:id/buy' => 'produtos#add'
  get 'produto_no_carrinhos/:id/remove' => 'produto_no_carrinhos#remove'
  root to: "produtos#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
