class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.boolean :admin
      t.boolean :atleta
      t.boolean :associado
      t.string :name

      t.timestamps
    end
  end
end
