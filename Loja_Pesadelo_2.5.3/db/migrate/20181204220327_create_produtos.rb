class CreateProdutos < ActiveRecord::Migration[5.2]
  def change
    create_table :produtos do |t|
      t.float :preco_1
      t.float :preco_2
      t.float :preco_3
      t.string :nome
      t.integer :quantidade
      t.string :imagem

      t.timestamps
    end
  end
end
