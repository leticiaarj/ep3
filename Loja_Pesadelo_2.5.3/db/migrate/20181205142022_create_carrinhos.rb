class CreateCarrinhos < ActiveRecord::Migration[5.2]
  def change
    create_table :carrinhos do |t|
      t.float :preco_final
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
