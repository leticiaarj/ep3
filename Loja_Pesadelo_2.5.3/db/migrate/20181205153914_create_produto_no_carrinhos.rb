class CreateProdutoNoCarrinhos < ActiveRecord::Migration[5.2]
  def change
    create_table :produto_no_carrinhos do |t|
      t.string :nome
      t.integer :quantidade
      t.float :preco_1
      t.float :preco_2
      t.float :preco_3
      t.references :carrinho
      t.timestamps
    end
  end
end
