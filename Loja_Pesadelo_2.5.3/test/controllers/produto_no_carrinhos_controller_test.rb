require 'test_helper'

class ProdutoNoCarrinhosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @produto_no_carrinho = produto_no_carrinhos(:one)
  end

  test "should get index" do
    get produto_no_carrinhos_url
    assert_response :success
  end

  test "should get new" do
    get new_produto_no_carrinho_url
    assert_response :success
  end

  test "should create produto_no_carrinho" do
    assert_difference('ProdutoNoCarrinho.count') do
      post produto_no_carrinhos_url, params: { produto_no_carrinho: { nome: @produto_no_carrinho.nome, preco_1: @produto_no_carrinho.preco_1, preco_2: @produto_no_carrinho.preco_2, preco_3: @produto_no_carrinho.preco_3, quantidade: @produto_no_carrinho.quantidade } }
    end

    assert_redirected_to produto_no_carrinho_url(ProdutoNoCarrinho.last)
  end

  test "should show produto_no_carrinho" do
    get produto_no_carrinho_url(@produto_no_carrinho)
    assert_response :success
  end

  test "should get edit" do
    get edit_produto_no_carrinho_url(@produto_no_carrinho)
    assert_response :success
  end

  test "should update produto_no_carrinho" do
    patch produto_no_carrinho_url(@produto_no_carrinho), params: { produto_no_carrinho: { nome: @produto_no_carrinho.nome, preco_1: @produto_no_carrinho.preco_1, preco_2: @produto_no_carrinho.preco_2, preco_3: @produto_no_carrinho.preco_3, quantidade: @produto_no_carrinho.quantidade } }
    assert_redirected_to produto_no_carrinho_url(@produto_no_carrinho)
  end

  test "should destroy produto_no_carrinho" do
    assert_difference('ProdutoNoCarrinho.count', -1) do
      delete produto_no_carrinho_url(@produto_no_carrinho)
    end

    assert_redirected_to produto_no_carrinhos_url
  end
end
