require "application_system_test_case"

class ProdutoNoCarrinhosTest < ApplicationSystemTestCase
  setup do
    @produto_no_carrinho = produto_no_carrinhos(:one)
  end

  test "visiting the index" do
    visit produto_no_carrinhos_url
    assert_selector "h1", text: "Produto No Carrinhos"
  end

  test "creating a Produto no carrinho" do
    visit produto_no_carrinhos_url
    click_on "New Produto No Carrinho"

    fill_in "Nome", with: @produto_no_carrinho.nome
    fill_in "Preco 1", with: @produto_no_carrinho.preco_1
    fill_in "Preco 2", with: @produto_no_carrinho.preco_2
    fill_in "Preco 3", with: @produto_no_carrinho.preco_3
    fill_in "Quantidade", with: @produto_no_carrinho.quantidade
    click_on "Create Produto no carrinho"

    assert_text "Produto no carrinho was successfully created"
    click_on "Back"
  end

  test "updating a Produto no carrinho" do
    visit produto_no_carrinhos_url
    click_on "Edit", match: :first

    fill_in "Nome", with: @produto_no_carrinho.nome
    fill_in "Preco 1", with: @produto_no_carrinho.preco_1
    fill_in "Preco 2", with: @produto_no_carrinho.preco_2
    fill_in "Preco 3", with: @produto_no_carrinho.preco_3
    fill_in "Quantidade", with: @produto_no_carrinho.quantidade
    click_on "Update Produto no carrinho"

    assert_text "Produto no carrinho was successfully updated"
    click_on "Back"
  end

  test "destroying a Produto no carrinho" do
    visit produto_no_carrinhos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Produto no carrinho was successfully destroyed"
  end
end
