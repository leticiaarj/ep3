# Exercício Programado 3

# Loja Pesadelo

## Funcionalidades disponíveis ao usuário
* Cadastrar 
```sh
    O usuário se cadastra e passa as suas informações baseadas na atlética, se é associado, atleta ou não associado.
```
* Adicioinar ao Carrinho
```sh
    Adiciona o produto desejado ao carrinho, onde ele será listado.
```
* Carrinho
```sh
    Apresenta os produtos adicionados ao carrinho e o valor total deles.
```
* Edição de perfil
* Edição dos dados
* Logout
```sh
    Sai do site e não deixa seus dados visíveis a outros usuários.
```

## Funcionalidades disponíveis ao Administrador
* Cadastrar 
```sh
    O usuário se cadastra e passa as suas informações baseadas na atlética, se é associado, atleta ou não associado.
```
* Adicioinar ao Carrinho
```sh
    Adiciona o produto desejado ao carrinho, onde ele será listado.
```
* Carrinho
```sh
    Apresenta os produtos adicionados ao carrinho e o valor total deles.
```
* Criar/Remover/Editar Produto
```
    Possibilita gerar um novo produto com nome, os 3 preços possíveis, a quantidade e uma foto ou removê-lo do catálogo.
```
* Edição de perfil
* Edição dos dados
* Logout
```sh
    Sai do site e não deixa seus dados visíveis a outros usuários.
```

## Bugs e Problemas
Diversos problemas foram encontrados durante a tentativa de junção do Front-end com o Back-end. No fim, eles ficaram separados, o Front-end na pasta TEMPLATE e o Back-end na pasta Loja Pesadelo.